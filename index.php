<?php 
	ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(E_ALL);
	
	require_once "db_connect.php";//соединение с БД
    require_once "ActiveUser.interface.php";//интерфейс ActiveUser
    require_once "Guest.class.php";//класс Guest
	require_once "Person.class.php";//родительский класс Person
	require_once "Student.class.php";//дочерный класс Student от Person
	require_once "Teacher.class.php";//дочерный класс Teacher от Person
	require_once "Administrator.class.php";//дочерный класс Administrator от Person
    require_once "UserGreater.class.php";//класс UserGreater

    try{
        $sql = 'SELECT id FROM peoples';//вызов id из таблици peoples
        $results = $pdo->query($sql);
    }catch(PDOException $e){
        echo "Ошибка получения данных: ".$e->getMessage();
        exit();
    }

    $peoples = array();
	foreach ($results as $result){//создание объектов класса Person методом фабрика
	    $peoples[] = Person::getInstance($result['id'], $pdo);
	}

    try{
        $sql = 'SELECT * FROM guests';//вызов данных из таблици guests
        $result = $pdo->query($sql);

    }catch(PDOException $e){
        echo "Ошибка получения данных: ".$e->getMessage();
        exit();
    }

    $guests = array();
    foreach($result as $guest){//создание экземпляров класса guests
        $guests[] = new Guest(
            $guest['id'],
            $guest['status'],
            $guest['first_name'],
            $guest['last_name'],
            $guest['phone'],
            $guest['email']
        );
    }
?>

<!DOCTYPE html>
<html>
<!-- HEADER START -->
<head>
	<title>Homework #14</title>

		<meta charset="utf-8">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

</head>
<!-- HEADER END -->
<body>
<!-- CONTENT START -->	
	<div style="height: 900px; margin: 20px">
		<h1>Homework #14</h1>
        <br>
	<!-- вывод списка людей -->
        <div>
            <h4>Список пользователей:</h4>
            <?php
                foreach($peoples as $people){
                    echo UserGreater::getStringUser($people); //метод вывода строки пользователя
                    echo '<br>';
                    echo '<br>';
                }

                foreach($guests as $guest){
                    echo UserGreater::getStringUser($guest); //метод вывода строки пользователя
                    echo '<br>';
                    echo '<br>';
                }
            ?>
        </div>
    </div>
	<!-- CONTENT END -->
	<div id="footer">
		<div class="panel panel-default" style="background-color: green">
			<div class="panel-body text-center">
				Shapovalov (c) 2017
			</div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>