<?php
	class Administrator extends Person{
		protected $schedule;
		
		public function __construct($status, $firstName, $lastName, $phone, $email, $schedule){
			parent::__construct($status, $firstName, $lastName, $phone, $email);
			$this->schedule = $schedule;
			
		}

        public function getFullName(){//метод возврата имени
            return $this->lastName." ".$this->firstName;
        }

        public function getStatus(){//метод возврата статуса
            return $this->status;
        }

	}
?>	