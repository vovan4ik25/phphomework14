<?php
    class Guest  implements ActiveUser{
        protected $id;
        protected $status;
        protected $firstName;
        protected $lastName;
        protected $phone;
        protected $email;

        public function __construct($id, $status, $firstName, $lastName, $phone, $email){
            $this->id = $id;
            $this->status = $status;
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            $this->phone = $phone;
            $this->email = $email;
        }

        public function getFullName(){//метод возврата имени
            return $this->lastName." ".$this->firstName;
        }

        public function getStatus(){//метод возврата статуса
            return $this->status;
        }

    }
?>