<?php
	class Teacher extends Person{
		protected $subject;
		
		public function __construct($status, $firstName, $lastName, $phone, $email, $subject){
			parent::__construct($status, $firstName, $lastName, $phone, $email);
			$this->subject = $subject;
			
		}

        public function getFullName(){//метод возврата имени
            return $this->lastName." ".$this->firstName;
        }

        public function getStatus(){//метод возврата статуса
            return $this->status;
        }

	}
?>	