-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 22 2017 г., 23:24
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `persons`
--

-- --------------------------------------------------------

--
-- Структура таблицы `guests`
--

CREATE TABLE `guests` (
  `id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `guests`
--

INSERT INTO `guests` (`id`, `status`, `first_name`, `last_name`, `phone`, `email`) VALUES
(1, 'guest', 'Вася', 'Серый', '35479257', 'ivanov@mail.ru'),
(2, 'guest', 'Коля', 'Петров', '56428735', 'petrov@mail.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `peoples`
--

CREATE TABLE `peoples` (
  `id` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `schedule` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `visit` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `peoples`
--

INSERT INTO `peoples` (`id`, `status`, `first_name`, `last_name`, `phone`, `email`, `schedule`, `subject`, `rating`, `visit`) VALUES
(1, 'student', 'Владимир', 'Шаповалов', '5555555', 'shapovalov@mail.ru', NULL, NULL, '[5,5,4,5,4,5]', '{\"01.03.2017\":false,\"05.03.2017\":true,\"12.03.2017\":true,\"15.03.2017\":false,\"19.03.2017\":true,\"22.03.2017\":false}'),
(2, 'teacher', 'Александр', 'Сосницкий', '6666666', 'sosnickiu@mail.ru', NULL, 'PHP', NULL, NULL),
(3, 'administrator', 'Кирил', 'Иванов', '7777777', 'ivanov@mail.ru', 'Пн., Ср., Пт.', NULL, NULL, NULL),
(4, 'student', 'Иван', 'Иванов', '74563812', 'ivanov@mail.ru', NULL, NULL, '[5,3,3,5,2,4]', '{\"01.03.2017\":false,\"05.03.2017\":false,\"12.03.2017\":true,\"15.03.2017\":false,\"19.03.2017\":false,\"22.03.2017\":false}'),
(5, 'student', 'Петр', 'Петров', '62873549', 'petr@mail.ru', NULL, NULL, '[5,5,5,5,5,5]', '{\"01.03.2017\":true,\"05.03.2017\":true,\"12.03.2017\":true,\"15.03.2017\":true,\"19.03.2017\":true,\"22.03.2017\":true}'),
(6, 'teacher', 'Вася', 'Ковалев', '12496358', 'vasy@mail.ru', NULL, 'java', NULL, NULL),
(7, 'teacher', 'Коля', 'Юшков', '68234896', 'koly@mail.ru', NULL, 'QA Automation', NULL, NULL),
(8, 'administrator', 'Женя', 'Коваль', '15637852', 'zheny@mail.ru', 'Вт., Ср., Пт.', NULL, NULL, NULL),
(9, 'administrator', 'Саша', 'Лопин', '56874358', 'sasha@mail.ru', 'Пн., Вт., Сб.', NULL, NULL, NULL),
(10, 'student', 'Виолетта', 'Васильева', '35492378', 'violeta@mail.ru', NULL, NULL, '[5,3,5,4,4,4]', '{\"01.03.2017\":false,\"05.03.2017\":false,\"12.03.2017\":false,\"15.03.2017\":true,\"19.03.2017\":true,\"22.03.2017\":true}');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `guests`
--
ALTER TABLE `guests`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `peoples`
--
ALTER TABLE `peoples`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `guests`
--
ALTER TABLE `guests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `peoples`
--
ALTER TABLE `peoples`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
