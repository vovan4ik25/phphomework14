<?php
	class Student extends Person{
        protected $rating;
        protected $visits;
        protected $midRating;
        protected $midVisit;

		public function __construct($status, $firstName, $lastName, $phone, $email, $rating, $visits){
			parent::__construct($status, $firstName, $lastName, $phone, $email);
			$this->rating = json_decode($rating, true);
			$this->visits = json_decode($visits, true);
            $this->midRating = $this->getMidRating();
            $this->midVisit = $this->getMidVisit();
		}

		 public function getMidRating(){//метод вычисление среднего балла
           	$suma = 0;
		 	foreach ($this->rating as $ratin){
		 		$suma+=$ratin;
		 	}
		 	return round($suma/count($this->rating),2);
		 }

		 public function getMidVisit(){//метод вычисления посещяимости
		 	$counter = 0;
		 	foreach ($this->visits as $visit){
		 		if($visit){
		 			$counter++;
		 		}
		 	}
		 	return round($counter/count($this->visits),2);
		 }

        public function getFullName(){//метод возврата имени
            return $this->lastName." ".$this->firstName;
        }

        public function getStatus(){//метод возврата статуса
            return $this->status;
        }

	}
?>	