<?php
    abstract class Person implements ActiveUser{
        protected $id;
        protected $status;
        protected $firstName;
        protected $lastName;
        protected $phone;
        protected $email;

        public function __construct($status, $firstName, $lastName, $phone, $email){
            $this->status = $status;
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            $this->phone = $phone;
            $this->email = $email;
        }

        public static function getInstance($id, PDO $pdo){//метод фабрика
            try{
                $sql1 = 'SELECT * FROM peoples WHERE id=:id';
                $stmt = $pdo->prepare($sql1);
                $stmt->bindValue(':id', $id);
                $stmt->execute();
                $result = $stmt->fetchObject();
            }catch(PDOException $e){
                echo "Ошибка получения данных: ".$e->getMessage();
                exit();
            }
            if(empty($result)) {
                return null;
            }
            if($result->status == 'administrator'){
                $people = new Administrator(
                    $result->status,
                    $result->first_name,
                    $result->last_name,
                    $result->phone,
                    $result->email,
                    $result->schedule
                );
            }else if($result->status == 'teacher'){
                $people = new Teacher(
                    $result->status,
                    $result->first_name,
                    $result->last_name,
                    $result->phone,
                    $result->email,
                    $result->subject
                );
            }else if($result->status == 'student'){
                $people = new Student(
                    $result->status,
                    $result->first_name,
                    $result->last_name,
                    $result->phone,
                    $result->email,
                    $result->rating,
                    $result->visit
                );
            }
            $people->setID($result->id);
            return $people;
        }

        public function setID($id){//метод установки id
            $this->id = $id;
        }

    }
?>